import pandas as pd
from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from scipy import stats
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

from matplotlib.ticker import FormatStrFormatter
import matplotlib.ticker as ticker

import warnings
warnings.filterwarnings('ignore')


### Підготовка даних ###


df = pd.read_csv(r'cars.csv')
df.rename(columns={'MSRP':'Price'}, inplace=True)

# Список даних
print(df.head())

# Найдорожчі автомобілі
print(df.nlargest(5, 'Price'))

# Список параметрів
print(df.info())

# Відповідні dtypes, які допомагають зменшити використання пам'яті
df = df.astype({'Engine Fuel Type':'category', 'Transmission Type':'category',
                'Vehicle Size':'category', 'Vehicle Style':'category'})

# Розподіл відсутніх даних
print('\n', df.isnull().sum())

# Список назв моделей з відсутнім ринковою категорією
missing_cat = df[df['Market Category'].isnull()]['Model'].drop_duplicates().tolist()


# Режим ринкової категорії кожної моделі
def cat_mode(model):
    mode = df[df['Model'] == model]['Market Category'].mode()
    if not (mode.empty):
        return mode.values[0]


# Модель та її найпоширенішу категорія
mode = {}
for model in missing_cat:
    mode.update({model: cat_mode(model)})

new_mode = {k: v for k, v in mode.items() if v is not None}

# Заміна кожного NaN на категорію режиму ринку
for model in new_mode:
    values = new_mode[model]
    df[df['Model'] == model] = df[df['Model'] == model].replace(np.nan, mode[model])

# Приклад найпоширенішої категорії для Honda Civic
print('\n', 'Most Honda Civic are', cat_mode('Civic'), 'vehicles', '\n')

# Відсутнє значення тип палива
print(df[df['Engine Fuel Type'].isnull()])

# Заміна значенням (звичайний неетильований)
df['Engine Fuel Type'].replace(np.nan, 'regular unleaded', inplace=True)

# Видалення електричних транспортних засобів
df.drop(df[(df['Engine Fuel Type']=='electric')].index, inplace=True)

# Значення кількісті циліндрів роторних двигунів замінюємо на середньостатистичне
print(df[df['Engine Cylinders'].isnull()].head())
engine_mean = round(df['Engine Cylinders'].astype('float').mean())
df['Engine Cylinders'].replace(np.nan, engine_mean , inplace=True)

# Значення кількості дверей для Ferrari FF замінюємо на 2
print(df[df['Number of Doors'].isnull()])

# По аннології до категорії ринку замінюємо значення потужності двигуна на середнє
print(df[df['Engine HP'].isnull()].head())

# Список назв моделей з відсутнім двигуном hp
missing_hp = df[df['Engine HP'].isnull()]['Model'].drop_duplicates().tolist()


# Середній  hp кожної моделі
def engine_mean(model):
    mean = round(df[df['Model'] == model]['Engine HP'].mean())
    return mean


# Замінити значення Nan на середнє значення
for model in missing_hp:
    df[df['Model'] == model] = df[df['Model'] == model].replace(np.nan, engine_mean(model))

# Приклад для Lincoln Continental
print('\n', 'The average Lincoln Continental power is', '{0:.0f}'.format(engine_mean('Continental')), 'HP', '\n')

# Список hp двигуна для Impala
hp_impala = df['Engine HP'].loc[df['Model']=='Impala'].tolist()

# Відокремимо числові та категоріальні значення
num_hp = [value for value in hp_impala if isinstance(value, float)]
cat_hp = [cat for cat in hp_impala if isinstance(cat, str)]

# Середня потужність
mean_hp_impala = round(np.mean(num_hp))

# Замінимо помилкові значення на середні
df.loc[df['Model']=='Impala'] = df[df['Model']== 'Impala'].replace(cat_hp[0], mean_hp_impala)

# Набір даних без відсутніх значень
print(df.isnull().sum(), '\n')

# Додаємо параметр віку авто
df['Age'] = 2017 - df['Year']

# Переводимо споживання палива у літр на 100 км
df[['highway MPG', 'city mpg']] = 235 / df[['highway MPG', 'city mpg']]

# Перейменовуєм нові стовпці та округлюєм їх
df.rename(columns={'highway MPG':'Highway L/100km', 'city mpg':'City L/100km'}, inplace=True)
df[['Highway L/100km', 'City L/100km']] = df[['Highway L/100km', 'City L/100km']].round(decimals=2)

# Підготовлений дата сет
print(df.head())


### Дослідження даних ###


# Залежність ціни від потужності двигуна
fig, ax = plt.subplots(figsize=(10, 5))

ax.scatter(df['Engine HP'], df['Price'], s=1, alpha=0.7)

# Установим назви для осей
plt.title('Price with respect to Engine HP')
plt.xlabel('Engine Horsepower')
plt.ylabel('Price')

# Роздільник для тисяч осей y
ax.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.0f}'))

plt.show()

# Розбиваємо дані для візуалізації даних
df_viz = df.loc[df['Price'] <= 100000]
df_hypercar = df.loc[df['Price'] > 100000]

# Розподіл цін
plt.figure(figsize=(10, 4))
plt.hist(df_viz['Price'], bins=80)

plt.title('Price distriution (for vehicles < 100K $)')
plt.xlabel('Price in $')
plt.ylabel('count')
plt.show()

# Діапазон ціни в залежності від розміру двигуна
fig, axes = plt.subplots(1, 2, figsize=(12,4))

# Робимо 2 ділянки із ціновою категорією
sns.boxplot(x='Engine Cylinders', y='Price', data=df_viz,
            ax=axes[0], palette="spring").set_title('Vehicles < 100K$')

sns.boxplot(x='Engine Cylinders', y='Price', data=df_hypercar,
            ax=axes[1], palette="PuRd").set_title('Vehicles > 100K$')

plt.tight_layout()
plt.subplots_adjust(wspace = 0.4)
plt.show()

# 2 найдешевші та найдорожчі 12-циліндрові машини
print(df_viz[df_viz['Engine Cylinders'] == 12].iloc[np.r_[0:2, -2:0]])

# Співвідношення кореляції між змінними
corr_map = df.corr()

# Маска для верхнього трикутника
mask = np.zeros_like(corr_map, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Намалюємо теплову карту
f, ax = plt.subplots(figsize=(10, 5))

sns.heatmap(corr_map, mask=mask, cmap='RdBu_r', vmax=1, center=0,
            square=True, linewidths=.5, ax=ax)

plt.xticks(rotation='60')
plt.title('Heatmap correlation of the numerical variables')

plt.show()

# Таблиця кореляції
print(df.corr())

df = df.drop(columns=['Year', 'Highway L/100km'], axis=1)

# Спостереження взаємозв'язків між змінними
sns.pairplot(df)

plt.show()

# Логарифм трансформації ціни
df['Log_price'] = df['Price'].apply(lambda x : np.log(x+1))

# Потужність
df['Sq_engine_hp'] = df['Engine HP'].apply(lambda x : np.square(x))
df['Sqrt_engine_hp'] = df['Engine HP'].apply(lambda x : np.sqrt(x))
df['Log_engine_hp'] = df['Engine HP'].apply(lambda x : np.log(x+1))

# Таблиця кореляції
corr = df.corr()
print(corr[['Price', 'Log_price']].loc[['Engine HP', 'Sq_engine_hp', 'Sqrt_engine_hp', 'Log_engine_hp'], :])

# Вік
df['Sq_age'] = df['Age'].apply(lambda x : np.square(x))
df['Sqrt_age'] = df['Age'].apply(lambda x : np.sqrt(x))
df['Log_age'] = df['Age'].apply(lambda x : np.log(x+1))

# Таблиця кореляції
corr = df.corr()
print(corr[['Price', 'Log_price']].loc[['Age', 'Sq_age', 'Sqrt_age', 'Log_age'], :])

# Розхід пального у місті L/100km
df['Sq_city'] = df['City L/100km'].apply(lambda x : np.square(x))
df['Sqrt_city'] = df['City L/100km'].apply(lambda x : np.sqrt(x))
df['Log_city'] = df['City L/100km'].apply(lambda x : np.log(x+1))

corr = df.corr()
print(corr[['Price', 'Log_price']].loc[['City L/100km', 'Sq_city', 'Sqrt_city', 'Log_city'], :])

# Відкинемо невикористані стовпці
df = df.drop(['Sq_engine_hp', 'Sqrt_engine_hp', 'Sqrt_age', 'Log_age', 'Sqrt_city', 'Log_city'], axis=1)

print('\n', df.loc[:,['Log_price', 'Log_engine_hp', 'Sq_age', 'Sq_city']].head(), '\n')


### Моделювання ###


# Проста лінійна регресія

# Визначення X і цільової змінної Y
X = df[['Log_engine_hp']]
Y = df[['Log_price']]

# Розділення набору даних в тренувальний і тестовий набір
# Ми будемо використовувати 80% набору даних для навчання і 20% для тестування
xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.2, random_state=53)

# Проста лінійна регресія з одним вхідним параметром Log_engine_hp
lr = LinearRegression()

lr.fit(xtrain, ytrain)

print('The slope is ', '{0:.4f}'.format(lr.coef_.item()))

print('The intercept is ', '{0:.4f}'.format(lr.intercept_.item()), '\n')

# Графік лінії прогнозування та її залишкової помилкі
fig, (ax1, ax2) = plt.subplots(ncols=2, sharey=False, figsize=(12, 5))

ax1= sns.regplot(x='Log_engine_hp', y='Log_price', data=df, scatter_kws={"s": 5}, ax=ax1)
ax1.set_title('Regression line of the Engine Cylinders')

ax2 = sns.residplot(X, Y, scatter_kws={"s": 3}, ax=ax2)
ax2.set_title('Residual plot')
ax2.set_ylabel('')
ax2.set_ylim(-5, 5)

plt.tight_layout()
plt.show()

# Зробимо перехресну перевірку на 4 підмножини
score = cross_val_score(lr, xtrain, ytrain, cv=4)

print('The cross-validations sets R-squared are :', score)
print('The average performance is therefore', np.mean(score), '\n')

# Зробимо деякі прогнози
yhat_simple = cross_val_predict(lr, xtest, ytest, cv=4)
print('The 1st prediction is', round(np.exp(yhat_simple[0].item())),
      'whereas the actual value was', df.Price[0], '\n')

# Оцінка моделі
simple_linear_R2_train = lr.score(xtrain, ytrain)
simple_linear_R2_test = lr.score(xtest, ytest)

# Ми трансформуємо yhat_simple для виконання тесту MSE
s_linear_MSE = mean_squared_error(ytest, np.exp(yhat_simple))

print('Simple Linear Regression', '\n')

print('Mean Squared Error', s_linear_MSE)
print('R-squared on train data', simple_linear_R2_train)
print('R-squared on test data', simple_linear_R2_test, '\n')

# Графік розподілу фактичних і прогнозованих значень
plt.figure(figsize=(10, 4))

ax1 = sns.distplot(yhat_simple, hist=False, label='Predicted Values')
sns.distplot(df['Log_price'], hist=False, label='Actual Values', ax=ax1)

plt.ylabel('Probability')
plt.legend()
plt.title('Predicted values using Simple Linear Regression')

plt.show()

# Поліноміальна регресія

# Функція для графічного зображення апроксимації поліноміальної регресійної моделі
def PlotPoly(model, independant_var, dependant_var):
    # Calculate the polynomial function for 100 points
    new_x = np.linspace(min(df[independant_var]), max(df[independant_var]), 1000)
    y_model = model(new_x)

    # Graph of the data points and the polynomial line
    plt.figure(figsize=(10, 5))
    plt.plot(X[independant_var], df[dependant_var], '.', markersize=1)
    plt.plot(new_x, y_model, '-')

    # Axis label and x-axis setting
    plt.xlabel(independant_var)
    plt.ylabel('Price')

    plt.show()
    plt.close()

# Встановлюємо змінні
X = df[['Engine HP']]
Y = df[['Price']]
xtrain, xtest, ytrain, ytest = train_test_split(X, Y, test_size=0.3, random_state=53)

# Обчислюємо поліноміальну функцію порядку 3
f = np.polyfit(xtrain['Engine HP'], ytrain['Price'], 3)
p = np.poly1d(f)  # наша модель

print('Polynomial function of the Engine Horsepower :', '\n')
print(p, '\n')

# Прогнозування
yhat_simple_poly = p(xtest)

print('Prediction', round(yhat_simple_poly[0].item()))
print('Actual value', df['Price'][0], '\n')

# Оцінка моделі
poly_R2_test = r2_score(ytest, yhat_simple_poly)

s_poly_MSE = mean_squared_error(ytest, yhat_simple_poly)

print ('Polynomial Regression')
print('R-square on the test data', poly_R2_test)
print('Mean Squared Error', s_poly_MSE, '\n')

# Графік лінії поліноміальної регресії
PlotPoly(p, 'Engine HP', 'Price')

# Графік розподілу фактичних і прогнозованих значень
plt.figure(figsize=(10, 4))

ax1 = sns.distplot(yhat_simple_poly, hist=False, label='Predicted Values')
sns.distplot(Y, hist=False, label='Actual Values', ax=ax1)

# Встановлюємо назвви осей та мітки
plt.title('Predicted values using Polynomial regression')
plt.xlim(-2500, 300000)
plt.xlabel('Price')
plt.ylabel('Probability')
plt.legend()

plt.show()

# Множинна лінійна регресія

# Лінійна регресія з кількома параметрами
lm = LinearRegression()

Z = df[['Log_engine_hp', 'Sq_age', 'Sq_city']]
Y = df['Log_price']

# Розподіляємо дані для навчання та тестування
ztrain, ztest, ytrain, ytest = train_test_split(Z, Y, test_size=0.3, random_state=53)

lm.fit(ztrain, ytrain)
print('Multiple Linear Regression', '\n')
print('Coefficient for each variable', lm.coef_)
print('Intercept term :', lm.intercept_, '\n')

# Прогнозування
yhat_multi = lm.predict(ztest)
print('Prediction :', round(np.exp(yhat_multi[0].item()), 0))
print('Actual value:', df.Price[0], '\n')

# Оцінка моделі
m_linear_R2 = lm.score(ztrain, ytrain)
m_linear_MSE = mean_squared_error(ytest, np.exp(yhat_multi))

print('Multiple Linear Regression')
print('Mean Squared Error', m_linear_MSE)
print('R-squared', m_linear_R2, '\n')

# Графік розподілу фактичних і прогнозованих значень
plt.figure(figsize=(10, 4))

ax1 = sns.distplot(yhat_multi, hist=False, label='Predicted Values')
sns.distplot(Y, hist=False, label='Actual Values', ax=ax1)

plt.ylabel('Proportion')
plt.legend()
plt.title('Predicted values using Multiple variables')

plt.show()

# Множинна поліноміальна регресія

# Встановлюємо параметри
Z = df[['Engine HP', 'Age', 'City L/100km']]
Y = df['Price']

ztrain, ztest, ytrain, ytest = train_test_split(Z, Y, test_size=0.3, random_state=53)

# Створення та навчання моделі
Input = [('scale', StandardScaler()), ('polynomial', PolynomialFeatures(degree=3)), ('model', LinearRegression())]

pipe = Pipeline(Input)
pipe.fit(ztrain, ytrain)

# Прогнозування
yhat_poly_multi = pipe.predict(ztest)

print('Prediction :', round(yhat_poly_multi[0].item(), 0))
print('Actual value :', df.Price[0], '\n')

# Оцінка моделі
m_poly_R2 = r2_score(ytest, yhat_poly_multi)
m_poly_MSE = mean_squared_error(ytest, yhat_poly_multi)

print('Multivariable Polynomial Regression')
print('Mean Squared Error', m_poly_MSE)
print('R-square', m_poly_R2, '\n')

# Графік розподілу фактичних і прогнозованих значень
plt.figure(figsize=(10, 4))

ax1 = sns.distplot(yhat_poly_multi, hist=False, label='Predicted Values')
sns.distplot(df['Price'], hist=False, label='Actual Values', ax=ax1)

plt.xlim(-2500, 200000)
plt.ylabel('Proportion')
plt.legend()
plt.title('Predicted values with a Polynomial Mulativariate model')

plt.show()







