Цей проект має на меті здійснити моніторинг та прогнозування вартості
автомобіля, використовуючи лінійну та поліноміальну регресію.

Використовувану базу даних автомобілів можна знайти на Kaggle наступним
посиланням: https://www.kaggle.com/CooperUnion/cardataset#data.csv.
Він містить більше 11 000 прикладів проданих автомобілів з
1990 - 2017 років, а також деякі їх характеристики.

Для запуску проекту потрібно:
- інтерпретатор Anaconda 3 2019.03 Python 3.7